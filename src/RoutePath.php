<?php
namespace Fulcrum\Router;

class RoutePath implements \JsonSerializable {

    protected $route;
    protected $params = [];
    protected $fullyQualified = false;

    public function __construct(Route $route)
    {
        $this->route = $route;
    }

    public function with($params=[]) {
        $this->params = $params;
        return $this;
    }

    public function fullyQualified($fullyQualified = true) {
        $this->fullyQualified = $fullyQualified;
        return $this;
    }

    public function __toString()
    {
        return $this->route->makePath($this->params, $this->fullyQualified);
    }

    /**
     * Specify data which should be serialized to JSON
     * @link https://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize()
    {
        return $this->__toString();
    }
}
