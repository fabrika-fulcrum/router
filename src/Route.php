<?php
namespace Fulcrum\Router;

use Fulcrum\Http\Request;
use Fulcrum\Http\RequestItem;
use Fulcrum\Http\Response;
use Fulcrum\Router\Exception\MalformedPatternException;

class Route
{
    const NAME_SEPARATOR = '.';
    const PATH_SEPARATOR = '/';
    const NAMESPACE_SEPARATOR = '\\';
    // Input
    protected $name;

    protected $method;
    protected $domain;

    protected $pattern;

    protected $constraints = [];

    //Structure
    /**
     * @var Route[]
     */
    protected $children = [];

    /**
     * @var Route
     */
    protected $parent;

    protected $resolved = false;


    //Operation

    protected $expression;

    protected $parameters = [];

    // Destination

    protected $namespace = '';

    protected $controller = '';

    protected $action = '';

    protected $asPost = false;

    /**
     * @param Route $item
     * @return Route
     */
    public function group(...$items)
    {

        foreach ($items as $item) {
            $this->addChild($item);
        }
        return $this;
    }

    public function g(...$items) {
        return $this->group(...$items);
    }

    /**
     * @return Route
     */
    public function asPost(){
        $this->asPost = true;
        return $this;
    }

    protected function addChild(Route $child)
    {
        $this->children[] = $child;
        $child->parent($this);
    }

    /**
     * @param null $parent
     * @return Route
     */
    public function parent($parent = null)
    {
        if (func_num_args() == 0) {
            return $this->getParent();
        }
        return $this->setParent($parent);
    }

    protected function setParent($parent)
    {
        $this->parent = $parent;
        return $this;
    }

    protected function getParent()
    {
        return $this->parent;
    }


    /**
     * @param mixed $name
     * @return Route|string
     */
    public function name($name = null)
    {
        if (func_num_args() == 0) {
            return $this->getName();
        }
        return $this->setName($name);

    }

    protected function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    protected function getName()
    {
        if ($this->parent) {
            return $this->joinNames($this->parent->name(), $this->name);
        } else {
            return $this->name;
        }
    }

    /**
     * @param mixed $method
     * @return Route|string
     */
    public function method($method = null)
    {
        if (func_num_args() == 0) {
            return $this->getMethod();
        }
        return $this->setMethod($method);
    }

    protected function setMethod($method)
    {
        $this->method = $method;
        return $this;
    }

    protected function getMethod()
    {
        if ($this->method) {
            return $this->method;
        } else if ($this->parent) {
            return $this->parent->method();
        } else {
            return $this->method;
        }
    }

    /**
     * @param mixed $domain
     * @return Route
     */
    public function domain($domain = null)
    {
        if (func_num_args() == 0) {
            return $this->getDomain();
        }
        return $this->setDomain($domain);
    }

    protected function setDomain($domain)
    {
        $this->domain = $domain;
        return $this;
    }

    protected function getDomain()
    {
        if ($this->domain) {
            return $this->domain;
        } else if ($this->parent) {
            return $this->parent->domain();
        } else {
            return $this->domain;
        }
    }

    /**
     * @param mixed $pattern
     * @return Route|string
     */
    public function pattern($pattern = null)
    {
        if (func_num_args() == 0) {
            return $this->getPattern();
        }
        return $this->setPattern($pattern);
    }

    protected function getPattern()
    {
        return $this->parent ?
            $this->joinPaths($this->parent()->pattern(), $this->pattern) :
            $this->pattern;
    }

    protected function setPattern($pattern)
    {
        $this->pattern = $pattern;
        return $this;
    }

    /**
     * @param string $namespace
     * @return Route|string
     */
    public function
    namespace($namespace = null)
    {
        if (func_num_args() == 0) {
            return $this->getNamespace();
        }
        return $this->setNamespace($namespace);
    }

    protected function getNamespace()
    {
        return $this->parent ?
            $this->joinNamespaces($this->parent()->namespace(), $this->namespace) :
            $this->namespace;
    }

    protected function setNamespace($namespace)
    {
        $this->namespace = $namespace;
        return $this;
    }

    /**
     * @param string $controller
     * @return Route|string
     */
    public function controller($controller = null)
    {
        if (func_num_args() == 0) {
            return $this->getController();
        }
        return $this->setController($controller);
    }

    protected function getController()
    {
        if ($this->controller) {
            return $this->controller;
        } else if ($this->parent) {
            return $this->parent->controller();
        } else {
            return $this->controller;
        }
    }

    protected function setController($controller)
    {
        $this->controller = $controller;
        return $this;
    }

    /**
     * @param string $action
     * @return Route|string
     */
    public function action($action = null)
    {
        if (func_num_args() == 0) {
            return $this->getAction();
        }
        return $this->setAction($action);
    }

    protected function getAction()
    {
        if ($this->action) {
            return $this->action;
        } else if ($this->parent) {
            return $this->parent->action();
        } else {
            return $this->action;
        }
    }

    protected function setAction($action)
    {
        $this->action = $action;
        return $this;
    }


    /**
     * Add a single Constraint or an array of key=>value constraints
     *
     * @param $paramName string|array
     * @param $constraint string|null
     * @return $this|
     */
    public function constrain($paramName, $constraint = null)
    {
        if (!is_array($paramName)) {
            return $this->constrain([$paramName => $constraint]);
        }
        foreach ($paramName as $name => $singleConstraint) {
            $this->constraints[$name] = $singleConstraint;
        }
        return $this;
    }

    /**
     * @param $constraints
     * @return $this|array
     */
    public function constraints($constraints = [])
    {
        if (func_num_args() == 0) {
            return $this->getConstraints();
        }
        return $this->setConstraints($constraints);
    }

    protected function getConstraints()
    {
        $result = $this->constraints;
        if ($this->parent) {
            $result = array_merge($this->parent()->constraints(), $result);
        }
        return $result;
    }

    protected function setConstraints($constraints)
    {
        $this->constrain($constraints);
        return $this;
    }

    protected function resolve()
    {
        if ($this->resolved) {
            return;
        }

        // Parameters

        $params = [];
        $matchCount = preg_match_all("/{([^{}]*)}/", $this->pattern(), $params);
        if ($matchCount === false) {
            throw new MalformedPatternException('Malformed Route Pattern in route ' . $this->name());
        }
        if ($matchCount > 0) {
            for ($i = 0; $i < $matchCount; $i++) {
                $this->parameters[$params[0][$i]] = $params[1][$i];
            }
        }

        ///calculate expression
        $defaultRegex = '([^/{}]+)';
        $pattern = $this->pattern();
        foreach ($this->parameters as $find => $replace) {
            if (array_key_exists($replace, $this->constraints)) {
                $pattern = str_replace($find, '(' . $this->constraints[$replace] . ')', $pattern);
            } else {
                $pattern = str_replace($find, $defaultRegex, $pattern);
            }
        }
        $this->expression = "/^" . str_replace('/', '\/', $pattern) . "\\/?$/";

        $this->resolved = true;
    }


    protected function isActionable()
    {
        return $this->controller() !== null && $this->action() !== null;
    }

    protected function testMatch(RequestItem $request)
    {
        $this->resolve();
        if (
            $this->isActionable()
            && ($this->domain() ? strtolower($request->domain()) == strtolower($this->domain()) : true)
            && $request->method() == $this->method()
            && preg_match($this->expression, $request->path())
        ) {
            return $this;
        }

        foreach ($this->children as $child) {
            $match = $child->matches($request);
            if ($match) {
                return $match;
            }
        }

        return false;
    }


    /**
     * @param RequestItem $request
     * @return bool|Route
     */
    public function matches(RequestItem $request)
    {
        $this->resolve();

        if ($this->method === '*' || $request->method() == $this->method) {
            if ($this->domain === null || $this->domain == $request->domain()) {
                if ($this->isActionable()) {
                    if (preg_match($this->expression, $request->path())) {
                        return $this;
                    }
                }
            }
		}
        if ($this->children) {
            foreach ($this->children as $child) {
                $match = $child->matches($request);
                if ($match) {
                    return $match;
                }
            }
        }
        return false;
    }

    public function getExpression()
    {
        $this->resolve();
        return $this->expression;
    }

    public function getParams(RequestItem $request)
    {
        $param_map = array_flip($this->parameters);
        $regex = $this->expression;
        $matches = preg_split($regex, $request->path(), null, PREG_SPLIT_DELIM_CAPTURE | PREG_SPLIT_NO_EMPTY);
        $params = [];
        foreach ($param_map as $key => $value) {
            $params[$key] = array_shift($matches);
        }
        return $params;
    }


    public function makePath($params = [], $fullyQualified = false)
    {
        $result = $this->pattern();
        if (is_object($params)) {
            $params = json_decode(json_encode($params), true);
        }

        foreach ($params as $key => $value) {
            $result = str_replace('{' . $key . '}', $value, $result);
        }

        $prefix = '';
        if ($fullyQualified) {
            $protocol = ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off') || $_SERVER['SERVER_PORT'] === 443) ? 'https://' : 'http://';
            $domainName = $_SERVER['HTTP_HOST'];
            $prefix = $protocol . $domainName;
        }
        return $prefix . $result;
    }

    /**
     * @param $name
     * @return Route|bool
     */
    public function find($name)
    {
        $thisName = $this->name();
        if ($name == $this->getName()) {
            return $this;
        }
        if (strlen($thisName) > strlen($name)) {
            return false;
        }
        if (substr($name, 0, strlen($thisName)) == $thisName) {
            foreach ($this->children as $child) {
                $found = $child->find($name);
                if ($found) {
                    return $found;
                }
            }
        }
        return false;
    }

    public function expose(&$var)
    {
        $var = $this;
        return $this;
    }

    private function joinPaths($part1, $part2)
    {
        return $this->glue($part1, $part2, static::PATH_SEPARATOR);
    }

    private function joinNames($part1, $part2)
    {
        return $this->glue($part1, $part2, static::NAME_SEPARATOR);
    }

    private function joinNamespaces($part1, $part2)
    {
        return $this->glue($part1, $part2, static::NAMESPACE_SEPARATOR);
    }

    private function glue($part1, $part2, $glue)
    {
        if (substr($part1, strlen($part1) - 1, 1) == $glue) {
            $part1 = substr($part1, 0, strlen($part1) - 1);
        }
        if (substr($part2, 0, 1) == $glue) {
            $part2 = substr($part2, 1);
        };
        return $part1 . $glue . $part2;
    }

    public function callAction(RequestItem $request, Response $response, $baseData=[]) {
    	if (!$this->isActionable()) {
    		return null;
		}
		$namespaces = [];
    	$namespace = $this->getNamespace();
    	if ($namespace) {
    		$namespaces = explode('\\',$namespace);
		}
        $nscontroller = [];
		foreach ($namespaces as $namespacePart) {
    	    if (!empty($namespacePart)) {
    	        $nscontroller[] = $namespacePart;
            }
        }
		$nscontroller[] = $this->getController();
    	for ($i = 0; $i< count($nscontroller); $i++){
    		$nscontroller[$i] = ucfirst($nscontroller[$i]);
		}
    	$controllerClass = implode("\\",$nscontroller).'Controller';
    	if (!class_exists($controllerClass)) {
    		throw new \Exception(sprintf('Could not find controller %s',$controllerClass));
		}
    	$method = $this->getAction();
        if ($this->method === Request::POST || $this->asPost) {
    	    $method.='Action';
        } else if ($this->method === Request::GET) {
            $method .= 'View';
        }

    	$rClass = new \ReflectionClass($controllerClass);
    	if (!$rClass->hasMethod($method)) {
			throw new \Exception(sprintf('Method %s not found in controller %s', $method, $controllerClass));
		}
		$rMethod = $rClass->getMethod($method);
    	if (!$rMethod->isPublic()) {
    		throw new \Exception(sprintf('Method %s in controller %s is not public', $method, $controllerClass));
		}
		$callArgs = [];
    	$params = $this->getParams($request);
    	$rParams = $rMethod->getParameters();
    	foreach ($rParams as $rParam) {
			if (isset($params[$rParam->getName()])) {
				$callArgs[] = $params[$rParam->getName()];
			} else {
				$callArgs[] = $rParam->getDefaultValue();
			}
		}
		$controller = new $controllerClass($request, $response);
    	return $controller->invokeMethod($method, $callArgs);
    	//return  call_user_func_array([$controller, $method], $callArgs);
	}

}
