<?php
namespace Fulcrum\Router;

class RouteManager
{

    protected $prefix = '';

    /**
     * @var Route[]
     */
    public $routes = [];

    protected $namedRoutes = [];
    protected $current;

    public function add(Route $route)
    {
        $route->manager($this);
        $this->routes[] = $route;
    }

    public function remove($name)
    {
        if (array_key_exists($name, $this->routes)) {
            unset($this->routes[$name]);
        }
    }

    /**
     * @param $request
     * @param bool $setAsCurrent
     * @return Route
     */
    public function findMatch($request, $setAsCurrent = false)
    {
        foreach ($this->routes as $route) {
            $match = $route->matches($request);
            if ($match !== false) {
                if ($setAsCurrent) {
                    $this->current = $match;
                    return $match;
                }
            }
        }
        return false;
    }

    /**
     * @return Route
     */
    public function current()
    {
        return $this->current;
    }

    /**
     * @param $name
     * @return Route
     * @throws \Exception
     */
    public function find($name)
    {
        if (array_key_exists($name, $this->namedRoutes)) {
            return $this->namedRoutes[$name];
        }
        throw new \Exception('Route name "' . $name . '" not found');
    }

    private function resolveNames()
    {
        $this->namedRoutes = [];
        foreach ($this->routes as $route) {
            /* @var Route $route */
            $this->namedRoutes[$route->name()] = $route;
        }
    }

    public function export($name)
    {
        $this->resolveNames();
        return $this->namedRoutes[$name]->getPublic();
    }

    public function showAll()
    {
        $this->resolveNames();
        return $this->namedRoutes;
    }

    public function getPublic()
    {
        $this->resolveNames();
        $result = [];
        foreach ($this->namedRoutes as $name => $route) {
            $result[$name] = $route->getPublic();
        }
        return $result;
    }
}
