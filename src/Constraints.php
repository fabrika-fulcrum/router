<?php
namespace Fulcrum\Router;

class Constraints
{
    const DIGITS = '\d+';
    const ALPHANUM = '[A-Za-z0-9]+';
    CONST SLUG = '[A-Za-z0-9][A-Za-z0-9-_]*';
    const ID = '[1-9]\d*';
    const IDS = '[1-9][\d+,]+\d|\d+';
    const STRID = '[0-9a-z]{20}';
    const ALPHA = '[A-Za-z]+';
    const FILEPATH = '(?!.*\.\.)[A-Za-z0-9/_\-\.]+';

    public static function Choice($array) {
        return implode('|',str_replace('|','\|',$array));
    }
}
