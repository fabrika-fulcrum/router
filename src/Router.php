<?php
namespace Fulcrum\Router;

use Fulcrum\Http\Request;

/**
 * Class Router
 * Static wrapper for RouteManager
 * @package Http
 */
class Router
{
	/** @var Route[] */
	protected static $roots = [];

	/** @var string */
	protected static $currentRoot;

	/** @var Route */
	protected static $current;

	public static function addRoot($rootName, Route $root) {
		static::$roots[$rootName] = $root;
		if (static::$currentRoot === null) {
		    static::$currentRoot = $rootName;
        }
		return static::$roots[$rootName];
	}

    /**
     * @return Route
     */
	public static function currentRoot(){
	    return static::$roots[static::$currentRoot];
    }

    public static function getRoot($rootName) {
	    return static::$roots[$rootName];
    }

    public static function setCurrentRoot($rootName) {
        static::$currentRoot = $rootName;
    }

    public static function current()
    {
		if (static::$current === null) {
		    foreach (static::$roots as $rootName=>$root) {
                $match = static::getRoot($rootName)->matches(Request::getCurrent());
                if ($match) {
                    static::$current = $match;
                    static::setCurrentRoot($rootName);
                }
            }
		}
		return static::$current;
    }

    public static function findMatch($uri, $setAsCurrent = false)
    {
        return static::currentRoot()->findMatch($uri, $setAsCurrent);
    }

    public static function path($name, $params = [], $fullyQualified = false) {
        $route = static::currentRoot()->find(static::currentRoot()->name().'.'.$name);
        if ($route) {
            $result = new RoutePath($route);
            $result->with($params);
            $result->fullyQualified($fullyQualified);
            return $result;
        }
        error_log('Warning: (PATH) Route not found: '.$name);
        return '';
    }

    public static function export($name)
    {
        return static::currentRoot()->export($name);
    }

    public static function showAll()
    {
        return static::currentRoot()->showAll();
    }

}
