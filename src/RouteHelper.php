<?php
namespace Fulcrum\Router;

use Fulcrum\Http\Request;

class RouteHelper {
    public static function post($pattern = '')
    {
        return static::create($pattern, Request::POST);
    }

    public static function get($pattern = '')
    {
        return static::create($pattern, Request::GET);
    }

    public static function getAction($pattern = '')
    {
        return static::create($pattern, Request::GET)->asPost();
    }

    public static function getAndPost($pattern='') {
        return static::create($pattern)->g(static::get(),static::post());
    }

    public static function put($pattern = '')
    {
        return static::create($pattern, Request::PUT);
    }

    public static function patch($pattern = '')
    {
        return static::create($pattern, Request::PATCH);
    }

    public static function delete($pattern = '')
    {
        return static::create($pattern, Request::DELETE);
    }

    public static function create($pattern = '', $method = '')
    {
        $result = new Route();
        $result->method($method);
        $result->pattern($pattern);
        return $result;
    }

    public static function controller($controller) {
        return static::create()->controller($controller);
    }

    public static function name($name) {
        return static::create()->name($name);
    }

    public static function namespace($namespace) {
        return static::create()->namespace($namespace);
    }

    public static function action($action) {
        return static::create()->action($action);
    }

    public static function path($pattern) {
        return static::create($pattern);
    }

    public static function domain($domain) {
        return static::create()->domain($domain);
    }

    public static function crud($name, $controller) {
        return static::create($name)->name($name)->controller($controller === null ? $name : $controller)->group(
            static::get()->name('list')->action('list'),
            static::get('create')->name('create')->action('create'),
            static::post('create')->name('create')->action('create'),
            static::get('{id}')->name('edit')->constrain('id',Constraints::ID)->action('edit'),
            static::post('{id}')->name('edit')->constrain('id',Constraints::ID)->action('edit'),
            static::get('{id}/delete')->name('delete')->constrain('id', Constraints::ID)->action('delete')
        );
    }

    public static function resource($name, $controller = null)
    {
        return static::create($name)->name($name)->controller($controller === null ? $name : $controller)->group(
            static::get()->action('list'),
            static::post()->action('create'),
            static::create('{id}')->constrain('id', Constraints::ID)->group(
                static::get()->name('view')->action('view'),
                static::put()->name('replace')->action('replace'),
                static::patch()->name('update')->action('update'),
                static::delete()->name('delete')->action('delete')
            )
        );
    }

}
